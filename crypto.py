import re
import operator


def decrypt(message, trigram):
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    m = find_m(message, trigram)
    subsets = []
    ic = []

    for _ in range(m):
        subsets.append("")

    for i in range(len(message)):
        subsets[i % m] += message[i]
    # So we have subsets, each column corresponds to a single letter of a key.
    # Get frequency of letters in each column, and compare frequencies

    for set in subsets:
        print(set)
        occurrences = compute_number_of_occurrences(set)
        ic.append(indices_of_coincidence(set, occurrences))

    print(ic)
    # Just for the test
    icm = {}
    for i in range(m - 1):
        for j in range(i + 1, m):
            icm[(i + 1, j + 1)] = mutual_ic(subsets[i], subsets[j])

    # print(icm)
    g = {}
    for el in icm:
        ic = max(icm[el])
        if ic >= 0.061:
            print(f"Most optimal IC is found in z{el} is {ic} at g={icm[el].index(ic)}")
            g[el] = icm[el].index(ic)

    # Found all g values
    print(g)
    print("Knowing that E=4 is the most freq letter, we're going to find z1")
    letter = max(compute_number_of_occurrences(subsets[0]).items(), key=operator.itemgetter(1))[0]
    print(f"Most frequent letter in the first subset: "
          f"{letter} with index {alpha.index(letter)}")
    print(
        f"Now shift 4 times back to get the first letter. {letter} -> {alpha[alpha.index(letter)-4]} is the first letter of the key.")
    print("From the system of modular equation, we obtain that the key is: z1, z1 + 12, z1 + 24, z1 + 9.\n"
          "With that, we know that z1 = 2 (C), hence, z2 = 2 + 12 = 14 (O), z3 = 2 + 24 = 0 (A), z4 = 11 (L).\n"
          "The key is COAL.")

    key = "COAL"

    return decrypt_with_key(message, key)


def decrypt_with_key(message, key):
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    decrypted = ""

    for i in range(len(message)):
        key_letter = key[i % len(key)]
        letter = message[i]
        decrypted += alpha[(alpha.index(letter) - alpha.index(key_letter)) % 26]

    return decrypted


def find_m(message, seq):
    indexes = [m.start() for m in re.finditer(seq, message)]
    # print(indexes)

    divisors = []

    for i in range(1, len(indexes) - 1):
        divisors.append(gcd(indexes[i], indexes[i + 1]))

    m = min(divisors)
    print(f"Key length is {m}")
    return m


# let us find greatest common divisor
def gcd(x, y):
    while y != 0:
        (x, y) = (y, x % y)
    return x


def indices_of_coincidence(text, occur):
    ic = 0
    N = len(text)

    for letter in occur:
        ic += (occur[letter] * (occur[letter] - 1))

    return ic / (N * (N - 1))


def compute_number_of_occurrences(substring):
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    occurrence = {}

    for a in alpha:
        occurrence[a] = 0

    for i in range(len(substring)):
        occurrence[substring[i]] += 1

    return occurrence


def mutual_ic(X, Y):
    # X - first column, Y - second column.
    occurrencesX = compute_number_of_occurrences(X)
    occurrencesY = compute_number_of_occurrences(Y)

    icm = []

    for g in range(26):
        ic = 0
        for l in range(26):
            keys = list(occurrencesX.keys())
            ic += ((occurrencesX[keys[l]] / len(X)) * (occurrencesY[keys[l - g]] / len(Y)))

        icm.append(ic)

    # print(ics)
    return icm


if __name__ == '__main__':
    encrypted = "VVEDGGIEWOTTQBSLUFENCZLPFPYANOTZCBDLUJIGKRLJCQTPFIPZPPYNQFTPBVAGGOCZOAOYCBDTPHECGGTTPUUYFSRWAWNRNCGTEBOE" \
                "KQEEJOTEJSSZNRIPTGACGBOEOCTTXOTPFHOCGHRPCHJFUHOCGJEYOOIYNMBJVVETTFAEKCNLNOSDGGSXGBTZHHHPFONRGFSZHPAEVZEL" \
                "PRBJVVETTGEWHWNEGFEDVFAEJSREJSYOKGCZXSRLUCUYFFELUCNEQFUYCKAJDMRPCZIKKBGEJOTHJOTTVAAVGGSPPGEQQFTSGATZFCDP" \
                "RSNOUCNHJOTTVKIWNAAVGGEYUSFZTCTSGFSEQROLPRTSCHAWNCFEJSOEJSRDEONYQHINGHHTUHOZGJEYCEUTVSBCCJEDQZDTGFMLADRP" \
                "HSREQFUYTOTSGFTSCBHPTCINCZLJDITAQWNENSSDNMDTGHRJKBGEQGTPOHHPQBCZOWNRVWDPCZLMAVIXUSLQVVUDYSCZWZDTOOGTPSWT" \
                "VVOFVQOYVFAOKQTTQBANKFCFOGTLPQETPKHTEVAYCFMJCZLZHKHZUSMPOPECUORPDFAGGTLPGGAEVCPDRSEODSFZTSTSGSNPOMMLMSSL" \
                "OCVPKTTSGGOWFWECUFELNZYLTSBCCJEEJSNEJWSDWFEWAWSYVHHPQITNQAELPMOQVVEXYONEGRELEVWZWZDSCJEATSFPTFEOVVAECZLD" \
                "VONOCBDQKUHEYVAEYSHLXSHPTSTSGBIDCQADGWNHJWCSVVETPHECCQTTQBOQOONJKBDTXWDFCZLJTOTTQBAWFSCTUWOYOOKTPUPCQQED" \
                "USSZPSPCQQEDUDECUCLOKSRATCDFESSLPCUEECMPKBTPPREODMNZQBEXQGTLTAIPUHRJVCAGQWDEJWSATCBWGAJFUHADECREGNDTFGIY" \
                "ESTSGMCLPHUDWOLWAAAVGFEETSAERVYDKQAWNMIXRCSDKPLPVVEJOOKPKHENQBOXKQAWNMIXRCSDKPLPVVEJUVOZVREDGFTPTGTSGBSE" \
                "CBDTPUAYFTIRJHIYIWSPCQHDQZDTGFSTPRIGKRULNZYCCHIZPOLNQIRDGCFLEHIZPOFEGFAWNPENCISPVVENQGTZHFUYPWNRKGSFTSTZ" \
                "DSAENSADVOSSKUHLUHHPECSEQTSECMIYI"
    trigram = "VVE"

    print(decrypt(encrypted, trigram))
