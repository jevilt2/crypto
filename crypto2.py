import random


def generate_primes(start, end):
    primes = []

    for i in range(start, end + 1):
        if i > 1:
            for n in range(2, i):
                if (i % n) == 0:
                    break
            else:
                primes.append(i)

    return primes


def get_random_primes():
    primes = generate_primes(1000, 2000)

    p = random.choice(primes)
    q = random.choice(primes)

    return p, q


def totient_function(p, q):
    return (p - 1) * (q - 1)


def select_public_exponent(euler):
    exps = []

    for i in range(1, euler):
        if gcd(i, euler) == 1:
            exps.append(i)

    return random.choice(exps)


def gcd(a, b):
    while b:
        a, b = b, a % b
    return a


# Implementation from https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
def extended_gcd(e, n):
    """return (g, x, y) such that a*x + b*y = g = gcd(a, b)"""
    if e == 0:
        return (n, 0, 1)
    else:
        g, x, y = extended_gcd(n % e, e)
        return g, y - (n // e) * x, x


def modinv(e, n):
    g, x, _ = extended_gcd(e, n)
    if g == 1:
        return x % n

def exponentiation(s, exp, n):
    return s ^ exp % n


if __name__ == '__main__':
    # (1931, 1487)
    p = 1931
    q = 1487
    n = p * q
    print(f"n = {n}")
    tot = totient_function(p, q)
    print(f"Euler's totient function value is {tot}")
    e = 2597043
    d = modinv(e, tot)
    print(f"e = {e}, d = {d}")
    print(f"Check: e * d = {e*d} = {e * d % tot} mod {tot}")
    s = 179556
    y = exponentiation(s, e, n)
    x = exponentiation(s, d, n)
    print(f"X has value {x}, Y has value {y}")
    print(f"Controll: x^e = {exponentiation(x, e, n)}, y^d = {exponentiation(y, d, n)}")


    # $x_1\equiv 1\cdot (-735) \cdot 1487 + 1486  \cdot 566 \cdot 1931 \mod 2871397$.
    print("\nTask 2 check")
    x_1 = (-735*1487 + 1486 * 566 * 1931) % n
    print(x_1)
    print(f"Check: {x_1**2%n}")
    x_2 = (-735*1487* 1930 + 1 * 566 * 1931) % n
    print(x_2)
    print(f"Check: {x_2**2%n}")
